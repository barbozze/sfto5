# Sfto5 - Автоматическая миграция пакета Symfony 3.4 в 5.2

***- ⚠️ Лучше идите сюда [rectorphp/rector (Instant Upgrades and Automated Refactoring)](https://github.com/rectorphp/rector)***

---

***- В моем случае это был переход с 3.4 на 5.2 и PHP 8.***

---

Это скромный инструмент для переноса пакетов `Sf3.4` в новую структуру каталога `Sf5.2`.
Эти инструменты специально разработаны как команды, поэтому вы можете адаптировать их под
ваш проект, когда это необходимо.

---

***- Это не даст вам 100% перехода, но избавит от рутины
и внесет небольшие полезные правки.***

---

### Описание инструментов
[Command/Sfto5/:][Sfto5]
- [Config][Config] - ️настройки для проектов, определение целей и исключений. Важно ⚠️
- [StartCommand][Start] - подготовка каталогов, кеша, временных изменений gitignore
- [StepConveyorCommand][Conveyor] - выполняет все шаги автоматически
- [Step1Command][Step1]
- [Step2Command][Step2]
- [Step3Command][Step3]
- [Step4Command][Step4]
- [Step5Command][Step5]
- [Step6Command][Step6]
- [Step7Command][Step7]
- [FinalCommand][Final]
- [CleanCommand][Clean]

### Подготовка старого проекта

- Если PhpStorm. кликаем ПКМ по каталог `/src`, затем кликаем «optimize imports». 
  Это удалит неиспользуемый импорт. Позже это потребуется.
- По возможности. Избавьтесь от группировок которые начинаются от корня пакета. 
  Плохой пример:
  ```php
  use App\SomeBundle\{
      Entity\Class, 
      Repository\Class, 
      Service\Class
  };
  ```
  Нужно привести к
  ```php
  use App\SomeBundle\Entity\Class;
  use App\SomeBundle\Repository\Class;
  use App\SomeBundle\Service\Class;
  ```

### Подготовка нового проекта

###### Шаг 1:

- Создайте новый проект. Тут есть вся необходимая информация 
  [Installing & Setting up the Symfony Framework][Sf:setup]
- Запустите *(опционально)*
  ```shell 
  $ symfony composer require symfony/filesystem;
  $ symfony composer require symfony/finder;
  $ symfony composer require symfony/yaml;
  ```

###### Шаг 2:

- Перенесите `app/src/Command/Sfto5` в ваш новый проект
- Запустите 
  ```shell
  $ bin/console sfto5:start;
  ```
  Вы должны получить похожий отчёт:
  
  ![i-1]

###### Шаг 3:

***- У вас должен появится каталог `target/src`***
- Если PhpStorm: *(опционально / рекомендую)*
  - Нужно пометить каталог `target/src` как исключенный. Что бы не помешало индексации
  - Откройте `Setting|Editor|FileTypes` и добавьте шаблон ассоциаций `*.php.target` для PHP. 
    Позже это даст вам ранний доступ для анализа кода.
    
###### Шаг 4:

- В `target/src` вам нужно скопировать ваши пакеты из старого проекта

###### Шаг 5: ⚠️

- Обязательно посмотрите настройки [Sfto5/ConfigInterface.php][Config] перед продолжением

###### Шаг 6:

- Запустите `bin/console sfto5:1;` или `bin/console sfto5:step-conveyor;`
- Для тех, кто спешит :) `sfto5:step-conveyor` 
  выполнит все шаги в автоматическом режиме

###### Шаг 7.1: *(опционально)*

- ***- До финала вы можете запустить очистку.*** Запустите
  ```shell
  $ bin/console sfto5:clean --collapse=t;
  ```
  произойдёт удаление `*.target` файлов, кеша, временных тегов и тд. 
  и придётся опять начать с `bin/console sfto5:start;` 

###### Шаг 7.2:

- ***- Если у вас всё хорошо прошло.*** Запустите
  ```shell
  $ bin/console sfto5:final;
  ```
  Произойдёт удаление `*.target` расширение у файлов, если потребуется вернуть 
   - Запустите `bin/console sfto5:final --rollback=t;`

- ***- В процессе могут появиться такие ошибки***
  
  ![i-2] 
  
  Файлы придётся открыть и поправить вручную, но если вас это всё же устраивает, запустите
  ```shell
  $ bin/console sfto5:final --cns=f;
  ```
  `--cns=f` это отключить проверку и произойдёт удаление `*.target` расширения

###### Шаг 8:

- ***- Если вы закончили.*** Запустите
  ```shell
  $ bin/console sfto5:clean;
  ```
  Это удалит временные теги `#SFTO-`, `@SFTO-`, метаданные, кэш, и тд

---

[Sf:setup]: https://symfony.com/doc/current/setup.html

[Config]: https://gitlab.com/barbozze/sfto5/-/blob/master/app/src/Command/Sfto5/ConfigInterface.php
[Sfto5]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5
[Start]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/StartCommand.php
[Conveyor]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/StepConveyorCommand.php
[Step1]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step1Command.php
[Step2]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step2Command.php
[Step3]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step3Command.php
[Step4]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step4Command.php
[Step5]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step5Command.php
[Step6]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step6Command.php
[Step7]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step7Command.php
[Final]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/FinalCommand.php
[Clean]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/CleanCommand.php

[i-1]: ./md-illustration/img_0.png
[i-2]: ./md-illustration/img_1.png
