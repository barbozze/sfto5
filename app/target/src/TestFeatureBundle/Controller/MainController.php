<?php

namespace Restoclub\TestFeatureBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/test")
 */
class MainController extends Controller
{
    public const TEST_TWIG_CONST = self::class . ': test twig const';

    /**
     * @Route("/a")
     * @Template
     */
    public function templateAction()
    {
        return ['template' => 'templateAction'];
    }

    /**
     * @Route("/aa")
     * @Template("TestFeatureBundle:Main:template.html.twig")
     */
    public function caseAAction()
    {
        return ['template' => 'templateAction'];
    }
}