<?php

namespace Restoclub\TestFeatureBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/test-admin")
 */
class TestAdminController extends Controller
{
    /**
     * This controller should not get into a new project
     * @see \App\Command\Sfto5\ConfigInterface::EXCLUDE_CONTROLLER_BY_SUFFIX
     */
}
