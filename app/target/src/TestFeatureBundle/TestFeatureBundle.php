<?php
namespace Restoclub\TestFeatureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TestFeatureBundle extends Bundle
{
    /**
     * This file will be ignored
     * @see \App\Command\Sfto5\Sfto5Abstract::ignoreBundleFile
     */
}
