<?php

namespace Restoclub\TestFeatureBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class ServiceB
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string
     */
    protected $str;

    /**
     * @var string
     */
    protected $parameter;

    public function __construct(EntityManager $em, Session $session, string $parameter)
    {
        $this->em      = $em;
        $this->session = $session;
        $this->parameter = $parameter;
    }

    public function setTemplate(string $str): void
    {
        $this->str = $str;
    }

    /**
     * @return string
     */
    public function generateToken(): string
    {
        return md5(uniqid((string)mt_rand(), true));
    }
}
