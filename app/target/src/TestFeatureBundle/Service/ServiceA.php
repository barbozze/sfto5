<?php

namespace Restoclub\TestFeatureBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class ServiceA
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string
     */
    protected $str;

    /**
     * @var string
     */
    protected $parameter;

    /**
     * @var ServiceB
     */
    protected $serviceB;

    public function __construct(EntityManager $em, Session $session, ServiceB $serviceB, string $parameter)
    {
        $this->em        = $em;
        $this->session   = $session;
        $this->parameter = $parameter;
        $this->serviceB  = $serviceB;
    }

    public function setTemplate(string $str): void
    {
        $this->str = $str;
    }

    /**
     * @return string
     */
    public function generateToken(): string
    {
        return md5(uniqid((string)mt_rand(), true)) . '_' . $this->serviceB->generateToken();
    }
}
