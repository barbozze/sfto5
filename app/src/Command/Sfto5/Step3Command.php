<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Step3Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 3;

    private array $deprecatedController = [
        'namespaceTarget' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\Controller',
        'namespace'       => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\AbstractController',
        'classExtends' => [
            'reg' => "/Controller\s*extends\s*Controller/m",
            'rep' => 'Controller extends AbstractController'
        ]
    ];

    private array $deprecatedRoute = [
        'namespaceTarget' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Configuration\\Route',
        'namespace'       => 'Symfony\\Component\\Routing\\Annotation\\Route',
    ];

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Replace deprecated 
          * <info>Controller</info> >> <comment>AbstractController</comment>
          * <info>Configuration\Route</info> >> <comment>Annotation\Route</comment>
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();

        foreach ($progressBar->iterate($metadata[self::MD_TARGET_FILE_INFO]) as $fileInfo) {
            if (isset($fileInfo[self::NAMESPACE])) {
                $filename = self::getPathToTargetFile($fileInfo);
                $contents = file_get_contents($filename);
                $isSaved  = false;

                $namespaceTarget = $this->deprecatedController['namespaceTarget'];
                if (str_contains($contents, $namespaceTarget)) {
                    $contents = str_replace($namespaceTarget, $this->deprecatedController['namespace'], $contents);
                    $classExtends = $this->deprecatedController['classExtends'];
                    $contents = preg_replace($classExtends['reg'], $classExtends['rep'], $contents);
                    $isSaved  = true;
                }

                $data = $this->deprecatedRoute;
                if (str_contains($contents, $data['namespaceTarget'])) {
                    $contents = str_replace($data['namespaceTarget'], $data['namespace'], $contents);
                    $isSaved  = true;
                }

                if ($isSaved) {
                    $filesystem->dumpFile($filename, $contents);
                }
            }
        }

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
