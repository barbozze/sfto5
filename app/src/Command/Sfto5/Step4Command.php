<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Step4Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 4;
    public  const OPTIONAL    = true;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Auto add <info>strict_types</info>
        - Fixed: No newline at end of file
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();
        $phpCode     = file_get_contents(__DIR__ . '/tmp/php-head-declare-strict-types.tmp');

        foreach ($progressBar->iterate($metadata[self::MD_TARGET_FILE_INFO]) as $fileInfo) {
            if (isset($fileInfo[self::NAMESPACE])) {
                $filename = self::getPathToTargetFile($fileInfo);
                $content = file_get_contents($filename);
                $content = str_replace('declare(strict_types=1);', '', $content);
                $content = preg_replace("/^\s*}\s*(?!\n)$/m", "}\n", $content);
                $content = preg_replace("/^<\?php\s*(?=namespace)/m", $phpCode, $content);
                $filesystem->dumpFile($filename, $content);
            }
        }

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
