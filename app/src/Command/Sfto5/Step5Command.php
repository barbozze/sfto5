<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Step5Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 5;
    public  const OPTIONAL    = true;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Mark <info>@Template(.*)</info> deprecated
        - Apply Attribute <comment>#[Template(.*)]</comment> & mark <bg=#777777;options=bold> #SFTO-AUTO-GEN </>
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();

        if (!self::PHP8_APPLY_TEMPLATED_ATTRIBUTES_INLAY) {
            $output->writeln(<<<'EOF'
            See Sfto5Interface::<fg=#886b8d;options=bold>PHP8_APPLY_TEMPLATED_ATTRIBUTES_INLAY</> = <fg=#bf702c;options=bold>FALSE</>
            The this step command will only work if <fg=#bf702c;options=bold>TRUE</>
            EOF);

            $output->writeln(self::passedSteps(self::STEP_NUMBER));

            return 0;
        }

        foreach ($progressBar->iterate($metadata[self::MD_TARGET_FILE_INFO]) as $fileInfo) {
            if (isset($fileInfo[self::NAMESPACE])) {
                $filename = self::getPathToTargetFile($fileInfo);
                $content  = file_get_contents($filename);
                $matches  = self::findAnnotationValAndMethod('Template', $content);
                if (count($matches) >= 4) {
                    foreach ($matches[3] as $k => $action) {
                        if (trim($matches[2][$k])) {
                            continue;
                        }

                        preg_match("/^(\s*)(.*function\s*$action\s*\(.*\))$/m", $content, $matchesAction);

                        if (!isset($matchesAction[1])) {
                            // TODO
                            continue;
                        }

                        $offset = $matchesAction[1]; # from the begin line
                        $defAction = preg_replace("/\s{2,}/", ' ', $matchesAction[2]);

                        $attr = null;
                        if ($logicalAlias = trim(trim($matches[1][$k]), "'\"")) { # we bring quotes to one type
                            $attr = "#[Template('$logicalAlias')]";
                        }

                        $applyAttr = [
                            $offset . ($attr ?? "#[Template]") . '#SFTO-AUTO-GEN',
                            "\n" . $offset . $defAction,
                        ];

                        $applyAttr = implode('', $applyAttr);
                        $content   = str_replace($matchesAction[0], $applyAttr, $content);

                        if (self::PHP7_REMOVE_TEMPLATED_ATTRIBUTES) {
                            $content = preg_replace(
                                "/(?<=\*)(\s*)(@Template)/", "$1TODO @SFTO-AUTO-REMOVE Template", $content
                            );
                        }

                        $filesystem->dumpFile($filename, $content);
                    }
                }
            }
        }

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
