<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StepConveyorCommand extends Sfto5Abstract
{
    protected function configure(): void
    {
        $this->setName('sfto5:step-conveyor');
        $this->setDescription(<<<'EOF'
        - Execute all step of Sfto
        EOF);
    }

    /**
     * @throws \JsonException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step conveyor: ');
        $output->writeln($this->getDescription());

        if (null === ($app = $this->getApplication())) {
            $message = ' Failed to get an instance of the application for this command. ';
            $margins = '<error>' . str_repeat(' ', strlen($message)) . '</error>';
            $output->writeln($margins);
            $output->writeln("<error>$message</error>");
            $output->writeln($margins);

            return 0;
        }

        $stepCommands = [];
        $numStep      = 1;

        while (true) {
            try {
                $stepCommands[] = $app->find('sfto5:' . $numStep);
            } catch (CommandNotFoundException) {
                break;
            }

            ++$numStep;
        }

        /** @var Command $command */
        foreach ($stepCommands as $command) {
            $greetInput = new ArrayInput([]);
            $command->run($greetInput, $output);
        }

        $output->writeln(PHP_EOL);

        return 0;
    }
}
