<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class CleanCommand extends Sfto5Abstract
{
    protected function configure(): void
    {
        $this->setName('sfto5:clean');

        $this->addOption(
            'collapse', null, InputOption::VALUE_OPTIONAL, 'Check use of old namespace?'
        );

        $this->setDescription(<<<'EOF'
        - Removing <bg=#777777;options=bold> #SFTO-{TAG} </>
        - Clearing cache
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Clean:');
        $output->writeln($this->getDescription());

        $metadata = self::getMetadata();

        $output->writeln("<comment>Removing gitignore rule</comment>");
        self::cleaningGitignore($metadata[self::MD_APP]);

        $isCollapse = $input->getOption('collapse') === 't';
        $filesystem = new Filesystem();

        if ($isCollapse) {
            $output->writeln("<error>Collapse: </error> deleting all <info>app/src/*.target</info> files");
            foreach ($metadata[self::MD_TARGET_FILE_INFO] as $fileInfo) {
                $filename = self::getPathToTargetFile($fileInfo);
                $filesystem->remove($filename);
            }
        } else {
            foreach ($metadata[self::MD_TARGET_FILE_INFO] as $fileInfo) {
                if (!isset($fileInfo[self::NAMESPACE])) {
                    continue;
                }
                $filename = $fileInfo[self::COPY_TO];
                $content  = file_get_contents($filename);
                $pattern  = "/(\n^.*TODO\s*@SFTO-AUTO-REMOVE.*$)/m";
                if (preg_match($pattern, $content)) {
                    $content = preg_replace($pattern, '', $content);

                }

                $needle  = "]#SFTO-AUTO-GEN";
                if (str_contains($content, $needle)) {
                    $content = str_replace($needle, ']', $content);
                    file_put_contents($filename, $content);
                }

                file_put_contents($filename, $content);
            }
        }

        $filename = $metadata[self::MD_APP_DEV_CACHE] . self::METADATA;
        if (is_file($filename)) {
            $output->writeln("<comment>Removing: $filename</comment>");
            $filesystem->remove($filename);
        }

        $filename = $metadata[self::MD_APP_DEV_CACHE] . self::TRASH_COLLECTOR . '_' . self::METADATA;
        if (is_file($filename)) {
            $output->writeln("<comment>Removing: $filename</comment>");
            $filesystem->remove($filename);
        }

        $output->writeln("<comment>Completed.</comment>" . PHP_EOL);

        return 0;
    }
}
