<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use App\Command\Sfto5\Exception\MetadataNotFoundException;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

abstract class Sfto5Abstract extends Command implements Sfto5Interface, ConfigInterface
{
    protected const OPTIONAL = false;

    protected static function passedSteps($n = 0): string
    {
        $x = count((new Finder())->files()->name("/Step\d+Command.php/")->in(__DIR__));

        $nn = $n + 1;
        $classname = __NAMESPACE__ . self::NAMESPACE_SEPARATOR . "Step{$nn}Command";
        $optional = ($nn <= $x && $classname::OPTIONAL)
            ? ' <bg=#c3c3c3;options=bold> <- (optional) </>'
            : '';

        $helper = ($n !== $x)
            ? "Run next: <info>bin/console sfto5:" . ($nn) . ";</info>" . $optional
            : '';

        $message = "Step: ($n/$x). " . $helper;

        if ($n === 0) {
            $message .= ' or <info>bin/console sfto5:step-conveyor;</info>';
        }

        if ($n === $x) {
            $message .= "And run <info>bin/console sfto5:final;</info>";
        } else {
            $message .= PHP_EOL . str_repeat('-', strlen(strip_tags($message)));
        }

        return PHP_EOL . "<comment>Completed.</comment>" . PHP_EOL . $message;
    }

    /**
     * @throws Exception
     */
    protected static function getMetadata(): array
    {
        $filename = dirname(__DIR__, 3) . self::P_APP_DEV_CACHE . self::METADATA;
        if (!is_file($filename)) {
            throw new MetadataNotFoundException('Not found: ' . $filename);
        }

        try {
            return json_decode(
                file_get_contents($filename), true, 512, JSON_THROW_ON_ERROR
            );
        } catch (Exception) {
            throw new Exception('Something wrong with the metadata. See ' . $filename);
        }
    }

    /**
     * @param string $autoloadPsr - See section in composer.json (autoload.[psr-0|psr-4])
     * @param string $path
     * @return string
     */
    protected static function getPathForConstructions(string $autoloadPsr, string $path): string
    {
        return $autoloadPsr
            . self::NAMESPACE_SEPARATOR
            . str_replace(
                DIRECTORY_SEPARATOR, self::NAMESPACE_SEPARATOR, trim($path, DIRECTORY_SEPARATOR)
            );
    }

    protected static function getNamespaceD(string $ns): string
    {
        return str_replace(self::NAMESPACE_SEPARATOR, self::NAMESPACE_SEPARATOR_D, $ns);
    }

    /**
     * nameName    >> name_name
     * nameNameA   >> name_name_a
     * nameNameABC >> name_name_abc
     */
    protected static function to_snake_case(string $str): string
    {
        return strtolower(preg_replace('/(?!^)(?<=[a-z])[A-Z]/', '_$0', trim($str)));
    }

    protected static function fillFilenameSnakeCase(array &$fileInfo): void
    {
        if (in_array($fileInfo[self::FILE_EXT], self::VIEW_EXT_TO_SNAKE_CASE, true)) {
            $fileInfo[self::FILENAME_SC] = self::to_snake_case($fileInfo[self::FILENAME]);
        }
    }

    public static function findAnnotationValAndMethod(string $annotation, string $contents): array
    {
        preg_match_all(
            "/(?<=@$annotation)(?:\s*\(\s*(.*)\))?;?\n(?:\s*.*?)+(?=\*\/)*(?:\s*?)(\#\[$annotation.*)*(?:\s*)(?:public\s+function)\s*(.*?)\s*(?:\()/",
            $contents,
            $matches
        );

        return $matches;
    }

    public static function isBundle(string $withoutRootPath): bool
    {
        return (bool) preg_match("/^\/.*Bundle/", $withoutRootPath);
    }

    public static function isBundleViews(string $path): bool
    {
        return str_contains($path, 'Bundle' . self::P_VIEWS);
    }

    public static function isAppViews(string $path): bool
    {
        return str_contains($path, self::P_TARGET_APP_TMP);
    }

    public static function ignoreBundleFile(array $fileInfo): bool
    {
        return $fileInfo[self::LEVEL] === 1;
    }

    /**
     * @throws Exception
     */
    public static function fillGitignore(string $path): void
    {
        $filesystem = new Filesystem();

        $rule  = "\n";
        $rule .= "#TODO <SFTO5-AUTO-GEN>\n";
        $rule .= "# Will be removed automatically\n";
        $rule .= "*." . self::TARGET_FILE_EXT . "\n";
        $rule .= self::P_TARGET . "\n";
        $rule .= "#TODO </SFTO5-AUTO-GEN>\n";

        $filename = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . '.gitignore';
        if (is_file($filename)) {
            if (is_writable($filename)) {
                if (!str_contains(file_get_contents($filename), $rule)) {
                    $filesystem->appendToFile($filename, $rule);
                }
            } else {
                throw new Exception("Failed to write file. See $filename");
            }
        } else {
            $filesystem->touch($filename);
            $rule = "#TODO SFTO5-AUTO-DELETE-THIS-FILE" . $rule;
            $filesystem->appendToFile($filename, $rule);
        }
    }

    /**
     * @throws Exception
     */
    public static function cleaningGitignore(string $path): void
    {
        $filename = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . '.gitignore';
        if (is_file($filename)) {
            $content = file_get_contents($filename);
            if (preg_match("/^#TODO SFTO5-AUTO-DELETE-THIS-FILE/", $content)) {
                $filesystem = new Filesystem();
                $filesystem->remove($filename);
                return;
            }

            $pattern = "/\n#TODO [<]SFTO5-AUTO-GEN[>]\n(.*\n)*#TODO [<]\/SFTO5-AUTO-GEN[>]\n/m";
            if (preg_match($pattern, $content)) {
                $content = preg_replace($pattern, '', $content);
                file_put_contents($filename, $content);
            }
        }
    }

    public static function getFilename(array $fileInfo): string
    {
        return $fileInfo[self::FILENAME_SC] ?? $fileInfo[self::FILENAME];
    }

    #[Pure]
    public static function getCopyToPath(array $fileInfo): string
    {
        $a = trim($fileInfo[self::ABS_ROOT_PATH], DIRECTORY_SEPARATOR);
        $b = trim($fileInfo[self::WITHOUT_ROOT_PATH], DIRECTORY_SEPARATOR);
        return DIRECTORY_SEPARATOR . $a . DIRECTORY_SEPARATOR . $b . DIRECTORY_SEPARATOR . self::getFilename($fileInfo);
    }

    /**
     * @throws \Exception
     */
    public static function getNamespaceBatch(array $fileInfo, int $depth = 3): array
    {
        if ($depth < 3) {
            /**
             * An example of bad grouping of imports
             * @example use App/NameBundle/{
             *     Entity/Class.php
             *     Service/Class.php
             * }
             * Such examples will not be converted, they will have to be corrected manually :(
             */
            throw new Exception("The value must be >= 3");
        }

        $eNsTarget = explode(self::NAMESPACE_SEPARATOR, $fileInfo[self::TARGET_NAMESPACE]);
        $eNs       = explode(self::NAMESPACE_SEPARATOR, $fileInfo[self::NAMESPACE]);
        $nsEol     = self::NAMESPACE_SEPARATOR . '{';
        $listNsBatch = [];

        foreach ($eNsTarget as $k => $ns) {
            if ($k < $depth) {
                continue;
            }
            $l = implode(self::NAMESPACE_SEPARATOR, array_slice($eNsTarget, 0, $k)) . $nsEol;
            $r = implode(self::NAMESPACE_SEPARATOR, array_slice($eNs, 0, $k)) . $nsEol;
            $listNsBatch[$l] = $r;
        }

        return $listNsBatch;
    }

    /**
     * @throws Exception
     */
    public static function getPathToTargetFile(array $fileInfo, bool $applyRollback = false): string
    {
        $filename = $fileInfo[self::COPY_TO] . ($applyRollback ? '' : '.' . self::TARGET_FILE_EXT);

        if (!is_file($filename)) {
            throw new Exception(
                'Not found: ' . $filename .
                "\n\e[94m> Maybe you need to rollback first, run: bin/console sfto5:final --rollback=t; ? \e[0m"
            );
        }

        return $filename;
    }

    public static function getLogicAliasSeparatedByColon(array $fileInfo): string
    {
        return
            ($fileInfo[self::TARGET_BUNDLE_NAME] ?? '') .
            ':' .
            trim($fileInfo[self::TARGET_WITHOUT_ROOT_PATH] , DIRECTORY_SEPARATOR) .
            ':' .
            $fileInfo[self::FILENAME];
    }

    public static function getLogicAliasByCommercialAt(array $fileInfo): string
    {
        return '@' .
            $fileInfo[self::BUNDLE_NAME] .
            $fileInfo[self::TARGET_WITHOUT_ROOT_PATH] .
            $fileInfo[self::FILENAME];
    }

    public static function getParamPlaceholder(string $str, $wrap = '%'): string
    {
        return "$wrap{$str}$wrap";
    }
}
