<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Step6Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 6;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Creates a logical alias namespace map
        - Replacing logical aliases (<info>@Name/</info>, <info>NameBundle:</info>, <info>::path</info>) etc.
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();
        $serviceYaml    = $metadata[self::MD_APP_DEV_CACHE] . self::SERVICES_YAML;
        $contentYaml    = file_get_contents($serviceYaml);
        $targetFileInfo = $metadata[self::MD_TARGET_FILE_INFO];

        $max = 0;
        foreach ($targetFileInfo as $fileInfo) {
            $path = $fileInfo[self::COPY_FROM];
            if (self::isBundleViews($path) || self::isAppViews($path)) {
                ++$max;
            }
        }

        $progressBar->setMaxSteps($max);
        $progressBar->start();

        foreach ($targetFileInfo as $fileInfo) {
            $cases = [];
            $path  = $fileInfo[self::COPY_FROM];
            if (self::isBundleViews($path)) {
                $progressBar->advance();

                $cases[] = self::getLogicAliasSeparatedByColon($fileInfo);
                $cases[] = self::getLogicAliasByCommercialAt($fileInfo);
            } elseif (self::isAppViews($path)) {
                $cases[] = self::getLogicAliasSeparatedByColon($fileInfo);

                if ($fileInfo[self::TARGET_WITHOUT_ROOT_PATH] !== DIRECTORY_SEPARATOR) {
                    $cases[] = ltrim($fileInfo[self::TARGET_WITHOUT_ROOT_PATH], DIRECTORY_SEPARATOR) .
                        $fileInfo[self::FILENAME];
                }

                if ($fileInfo[self::TARGET_WITHOUT_ROOT_PATH] === DIRECTORY_SEPARATOR) {
                    $cases[] = $fileInfo[self::FILENAME];
                }
            }

            if (count($cases)) {
                $twigQuotes    = self::TWIG_TYPE_QUOTES_WHEN_EXTENDS_AND_INCLUDE;
                $yamlQuotes    = self::YAML_TYPE_QUOTES;
                $path          = $fileInfo[self::WITHOUT_ROOT_PATH] . self::getFilename($fileInfo);
                $replaceInTwig = $twigQuotes . $path . $twigQuotes;
                $replaceInYaml = $yamlQuotes . $path . $yamlQuotes;

                foreach ($targetFileInfo as $fileInfo1) {
                    $filename = self::getPathToTargetFile($fileInfo1);
                    $content  = file_get_contents($filename);
                    $isSaved  = false;

                    foreach ($cases as $case) {
                        $caseA = "'$case'";
                        if (str_contains($content, $caseA)) {
                            $content = str_replace($caseA, $replaceInTwig, $content);
                            $isSaved  = true;
                        }

                        $caseB = "\"$case\"";
                        if (str_contains($content, $caseB)) {
                            $content = str_replace($caseB, $replaceInTwig, $content);
                            $isSaved  = true;
                        }

                        $contentYaml = str_replace(
                            [$caseA, $caseB, $case], $replaceInYaml, $contentYaml
                        );
                    }

                    if ($isSaved) {
                        $filesystem->dumpFile($filename, $content);
                    }
                }
            }
        }

        file_put_contents($serviceYaml, $contentYaml);

        $progressBar->finish();

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
