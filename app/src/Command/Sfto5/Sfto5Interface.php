<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

interface Sfto5Interface
{
    public const PHP8_APPLY_TEMPLATED_ATTRIBUTES_INLAY = true; # Attribute update @Template >> #[Template]
    public const PHP7_REMOVE_TEMPLATED_ATTRIBUTES      = true; # Removing @Template

    # ------------------------------------------------------------------------------------------------------------------

    /**
     * Assigning a bundle name to parameters
     */
    public const YAML_ALLOW_PREFIX_PARAM     = true;
    public const YAML_PARAM_SEPARATOR_BEFORE = '__sfto_param__';
    public const YAML_PARAM_SEPARATOR_AFTER  = '__';

    # ------------------------------------------------------------------------------------------------------------------

    public const NAMESPACE_SEPARATOR         = '\\';
    public const NAMESPACE_SEPARATOR_D       = '\\\\';
    public const TARGET_FILE_EXT             = 'target';
    public const METADATA                    = 'metadata.json';
    public const SERVICES_YAML               = 'services.yaml';
    public const SERVICES_YAML_DIST          = 'services.yaml.dist';

    # ------------------------------------------------------------------------------------------------------------------

    public const P_VIEWS          = '/Resources/views/';
    public const P_CONFIG         = '/Resources/config/';
    public const P_TARGET         = '/target/';
    public const P_TARGET_SRC     = '/target/src/';
    public const P_TARGET_APP_TMP = '/target/app' . self::P_VIEWS;

    public const P_APP_SRC        = '/src/';
    public const P_APP_CONFIG     = '/config/';
    public const P_APP_TMP        = '/templates/';
    public const P_APP_DEV_CACHE  = '/var/cache/dev/Sfto5/';

    # ------------------------------------------------------------------------------------------------------------------

    public const VIEW_EXT_TO_SNAKE_CASE = ['twig', 'html', 'json'];

    # ------------------------------------------------------------------------------------------------------------------

    public const GROUP_TARGET_SRC        = 'target_src';
    public const GROUP_TARGET_APP_TMP    = 'target_app_tmp';
    public const GROUP_TARGET_BUNDLE_TMP = 'target_bundle_tmp';

    # ------------------------------------------------------------------------------------------------------------------
    # Ket target file info

    public const GROUP                    = 'group';
    public const LEVEL                    = 'level';

    public const CONTENT                  = 'content';
    public const FILE_EXT                 = 'file_ext';
    public const FILENAME                 = 'filename';
    public const FILENAME_SC              = 'filename_snake_case';

    public const TARGET_ABS_ROOT_PATH     = 'asb_root_path_target';
    public const ABS_ROOT_PATH            = 'asb_root_path';

    public const TARGET_BUNDLE_NAME       = 'bundle_name_target';
    public const TARGET_BUNDLE_NAME_OLD   = 'old_bundle_name_target';
    public const BUNDLE_NAME              = 'bundle_name';

    public const TARGET_NAMESPACE         = 'namespace_target';
    public const NAMESPACE                = 'namespace';

    public const TARGET_WITHOUT_ROOT_PATH = 'without_root_path_target';
    public const WITHOUT_ROOT_PATH        = 'without_root_path';

    public const COPY_FROM                = 'copy_from';
    public const COPY_TO                  = 'copy_to';

    # ------------------------------------------------------------------------------------------------------------------

    public const MD_APP              = 'md_app';
    public const MD_TARGET_SRC       = 'md_target_src';
    public const MD_TARGET_APP_TMP   = 'md_target_app_tmp';
    public const MD_APP_SRC          = 'md_app_src';
    public const MD_APP_TMP          = 'md_app_tmp';
    public const MD_APP_CONFIG       = 'md_app_config';
    public const MD_APP_DEV_CACHE    = 'md_app_cache';
    public const MD_TARGET_FILE_INFO = 'md_target_file_info';
    public const MD_SERVICE_ID       = 'md_services_id';

    # ------------------------------------------------------------------------------------------------------------------

    public const TRASH_COLLECTOR = 'trash_collector';

    # ------------------------------------------------------------------------------------------------------------------

    public const TWIG_TYPE_QUOTES_WHEN_EXTENDS_AND_INCLUDE = '"';
    public const YAML_TYPE_QUOTES = '\'';
}
