<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use function count;

class Step7Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 7;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Replacing     <info>service ID</info> >> <comment>service class</comment>
        - Replacing <info>parameter name</info> >> <comment>parameter compound name</comment>
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $metadata    = self::getMetadata();
        $filename    = $metadata[self::MD_APP_DEV_CACHE] . self::SERVICES_YAML;
        $contentYaml = file_get_contents($filename);
        $serviceYaml = Yaml::parseFile($filename);

        foreach ($serviceYaml['parameters'] as $key => $value) {
            $eKey = explode(self::YAML_PARAM_SEPARATOR_BEFORE, $key);
            if (count($eKey) === 2) {
                $contentYaml = str_replace(
                    Sfto5Abstract::getParamPlaceholder($eKey[1]),
                    Sfto5Abstract::getParamPlaceholder($key),
                    $contentYaml
                );
            }
        }

        foreach ($serviceYaml['services'] as $class => $service) {
            if ($serviceID = $service[self::MD_SERVICE_ID] ?? null) {
                $contentYaml = str_replace($serviceID, $class, $contentYaml);
            }
        }

        $mdKey = self::MD_SERVICE_ID;
        $contentYaml = preg_replace("/^\s*($mdKey):.*$/m", "", $contentYaml);
        $contentYaml = str_replace(self::YAML_PARAM_SEPARATOR_BEFORE, self::YAML_PARAM_SEPARATOR_AFTER, $contentYaml);

        file_put_contents($filename, $contentYaml);

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
