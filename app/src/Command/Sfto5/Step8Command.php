<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Step8Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 8;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Replacing the old route prefix 
        EOF);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $outputStyle = new OutputFormatterStyle('red', '#ff0', ['bold', 'blink']);
        $output->getFormatter()->setStyle('fire', $outputStyle);

        $output->writeln(PHP_EOL . $this->getName() . ': <fire> in development </>');

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
