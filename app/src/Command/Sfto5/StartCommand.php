<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class StartCommand extends Sfto5Abstract
{
    protected function configure(): void
    {
        $this->setName('sfto5:start');

        $this->setDescription(<<<'EOF'
        Preparing the required directories and files:
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start:');
        $output->writeln($this->getDescription());

        $root = dirname(__DIR__, 3);

        $targetAppTmp = $root . self::P_TARGET_APP_TMP;
        $targetSrc    = $root . self::P_TARGET_SRC;
        $appSrc       = $root . self::P_APP_SRC;
        $appTmp       = $root . self::P_APP_TMP;
        $appConfig    = $root . self::P_APP_CONFIG;
        $appCache     = $root . self::P_APP_DEV_CACHE;
        $servicesYaml     = $appConfig . self::SERVICES_YAML;
        $servicesYamlDist = $appConfig . self::SERVICES_YAML_DIST;

        foreach ([$appSrc, $appTmp, $appConfig, $servicesYaml] as $path) {
            $err = 'Required directory or file.';

            if (!is_readable($path)) {
                throw new Exception("$err Not allowed to read: $path");
            }

            if (!is_writable($path)) {
                throw new Exception("$err Not allowed to write: $path");
            }
        }

        $filesystem = new Filesystem();
        foreach ([$targetAppTmp, $targetSrc, $appCache] as $directory) {
            $output->writeln("- <info>$directory</info>");
            $filesystem->mkdir($directory);
        }

        $metadata = [
            self::MD_APP              => $root,
            self::MD_APP_CONFIG       => $appConfig,
            self::MD_TARGET_APP_TMP   => $targetAppTmp,
            self::MD_TARGET_SRC       => $targetSrc,
            self::MD_APP_SRC          => $appSrc,
            self::MD_APP_TMP          => $appTmp,
            self::MD_APP_DEV_CACHE    => $appCache,
            self::MD_TARGET_FILE_INFO => [],
        ];

        $output->writeln("- <comment>$root/.gitignore</comment> (modified/created)");
        self::fillGitignore($root);

        $filename = $appCache . self::METADATA;
        $output->writeln("- <comment>$filename</comment>");
        $filesystem->dumpFile($filename, json_encode($metadata, JSON_THROW_ON_ERROR | true));

        if (!is_file($servicesYamlDist)) {
            $output->writeln("- <info>$servicesYaml</info> >> <comment>$servicesYaml.dist</comment>");
            $filesystem->copy($servicesYaml, $servicesYamlDist);
        }

        $output->writeln(self::passedSteps());

        return 0;
    }
}
