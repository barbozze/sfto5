<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

interface ConfigInterface
{
    /**
     * See section in composer.json (autoload.[psr-0|psr-4])
     */
    public const AUTOLOAD_PSR_OLD = 'Restoclub'; // [i] Replace with your name old
    public const AUTOLOAD_PSR_NEW = 'App';

    # ------------------------------------------------------------------------------------------------------------------

    /**
     * old-project/app/Resources/views/ >> app/templates/{P_COMMON_TMP_DIR}
     */
    public const P_COMMON_TMP_DIR = 'common';

    # ------------------------------------------------------------------------------------------------------------------

    /**
     * @var array|string[] - Excluded bundle
     * @example ['NameBundle',];
     */
    public const EXCLUDE_BUNDLE = [
        'TestFeatureBundle',
    ];

    /**
     * @var array - list to rename bundle
     * @example ['CoreBundle' => 'BaseBundle',];
     */
    public const RENAME_BUNDLE = [
        // 'TestFeatureBundle' => 'TestTargetBundle',
    ];

    /**
     * Bundle to be processed
     * @var array|string[] - leave array empty if you need to process all bundle
     * @example ['NameBundle',];
     */
    public const TARGET_BUNDLE = [
        'TestFeatureBundle',
    ];

    # ------------------------------------------------------------------------------------------------------------------

    /**
     * @var array|string[] - Excluded / deprecated directories
     * @example ['Admin',];
     */
    public const EXCLUDE_BUNDLE_DIR = [
        'Resources', // <-- Must be excluded
        'Admin',     // <-- Optional. For example, the SonataAdmin has not yet been released under PHP8|Sf5.2
    ];

    /**
     * @var array - list to rename first level subdirectories
     * @example ['Services' => 'Service',];
     */
    public const RENAME_BUNDLE_DIR = [
        'Exceptions' => 'Exception',
        'Interfaces' => 'Interface',
        'Services'   => 'Service',
    ];

    /**
     * Directories to be processed
     * @var array|string[] - leave array empty if you need to process all directories
     */
    public const TARGET_BUNDLE_DIR = [
        // 'Controller',
        // 'Resources',
        // 'Command',
        // 'DependencyInjection',
        // 'DataTransformer',
        // 'Repository',
        // 'Exceptions', 'Exception', // FIXME by adding RENAME_BUNDLE_DIR
        // 'Annotation',
        // 'Interfaces', 'Interface', // FIXME by adding RENAME_BUNDLE_DIR
        // 'Validator',
        // 'Converter',
        // 'Services', 'Service', // FIXME by adding RENAME_BUNDLE_DIR
        // 'Listener',
        // 'Search',
        // 'Entity',
        // 'Traits',
        // 'Event',
        // 'Admin',
        // 'Form',
        // 'Type',
        // 'Util',
    ];

    # ------------------------------------------------------------------------------------------------------------------

    /**
     * @var array|string[] - Excluded controller by suffix
     * @example ['AdminController.php',];
     */
    public const EXCLUDE_CONTROLLER_BY_SUFFIX = [
        'AdminController.php',
    ];
}
