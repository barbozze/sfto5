<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class FinalCommand extends Sfto5Abstract
{
    protected function configure(): void
    {
        $this->setName('sfto5:final');

        $this->addOption('rollback', null, InputOption::VALUE_OPTIONAL, 'Apply rollback: t|f');
        $this->addOption('cns', null, InputOption::VALUE_OPTIONAL, 'Check use of old namespace: t|f');

        $this->setDescription(<<<'EOF'
        - Checking the use of the old namespace
        - Removing a <info>*.target</info> extension
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Final processing:');
        $output->writeln($this->getDescription());

        $isRollback = $input->getOption('rollback') === 't';
        $checkingUseOldNs = $input->getOption('cns') !== 'f';
        $isFinal = true;

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();
        $targetFileInfo = $metadata[self::MD_TARGET_FILE_INFO];

        // Checking the use of the old namespace
        if (!$isRollback && $checkingUseOldNs) {
            $regx = self::AUTOLOAD_PSR_OLD . '\\\\.*Bundle(\\\\)?';

            foreach ($targetFileInfo as $fileInfo) {
                $filename = self::getPathToTargetFile($fileInfo);
                $content  = file_get_contents($filename);

                if (preg_match("/$regx/m", $content)) {
                    $output->writeln("<error>ERROR: Using the old namespace. See $filename </error>");
                    $isFinal = false;
                }
            }

            if (!$isFinal) {
                $output->writeln("<info>Correct these files, they contain: <bg=yellow;options=bold> $regx </></info>");
                return 0;
            }
        }

        foreach ($progressBar->iterate($targetFileInfo) as $fileInfo) {
            $originFile = self::getPathToTargetFile($fileInfo, $isRollback);
            $targetFile = $originFile;

            $ext = '.' . self::TARGET_FILE_EXT;
            if ($isRollback) {
                $targetFile .= $ext;
            } else {
                $targetFile = str_replace($ext, '', $targetFile);
            }

            if (!is_file($originFile)) {
                throw new Exception('Not found: ' . $targetFile);
            }

            $filesystem->rename($originFile, $targetFile);
        }

        $appServiceYaml = $metadata[self::MD_APP_CONFIG] . self::SERVICES_YAML;
        $serviceYaml = $isRollback
            ? $metadata[self::MD_APP_CONFIG] . self::SERVICES_YAML_DIST
            : $metadata[self::MD_APP_DEV_CACHE] . self::SERVICES_YAML;

        file_put_contents($appServiceYaml, file_get_contents($serviceYaml));

        $message = $isRollback
            ? ""
            : "To rollback run: <info>bin/console sfto5:final</info> <comment>--rollback=t;</comment>\n" .
              "Or last run: <info>bin/console sfto5:clean;</info>";

        $output->writeln(PHP_EOL . $message);

        return 0;
    }
}
