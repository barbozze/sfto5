<?php

namespace App\Command\Sfto5\Exception;

use Exception as BaseException;

class MetadataNotFoundException extends BaseException
{

}