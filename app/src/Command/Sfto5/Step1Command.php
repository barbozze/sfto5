<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class Step1Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 1;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
         - Create directories
         - Create <info>app/src/*.target</info> files
         - Create <info>app/templates/*.target</info> files
         - Create <info>*.target</info> mapping
         - Create namespace mapping
         - Merge services & parameters <comment>(Preparation. Later there will be replacement of old names)</comment>
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $metadata = self::getMetadata();

        $targetFileInfo = $this->filesAssist([
            self::GROUP_TARGET_SRC     => $metadata[self::MD_TARGET_SRC],
            self::GROUP_TARGET_APP_TMP => $metadata[self::MD_TARGET_APP_TMP],
        ], [
            self::GROUP_TARGET_SRC     => $metadata[self::MD_APP_SRC],
            self::GROUP_TARGET_APP_TMP => $metadata[self::MD_APP_TMP],
        ], $output);

        $filesystem = new Filesystem();
        $filesystem->dumpFile(
            $metadata[self::MD_APP_DEV_CACHE] . self::TRASH_COLLECTOR . '_' . self::METADATA,
            json_encode($targetFileInfo[self::TRASH_COLLECTOR], JSON_THROW_ON_ERROR | true)
        );

        unset($targetFileInfo[self::TRASH_COLLECTOR]);
        $metadata[self::MD_TARGET_FILE_INFO] = $targetFileInfo;

        $this->serviceAssist($metadata);

        $filesystem->dumpFile(
            $metadata[self::MD_APP_DEV_CACHE] . self::METADATA,
            json_encode($metadata, JSON_THROW_ON_ERROR | true)
        );

        $progressBar = new ProgressBar($output);
        foreach ($progressBar->iterate($targetFileInfo) as $fileInfo) {
            $filesystem->copy(
                 $fileInfo[self::COPY_FROM],
                $fileInfo[self::COPY_TO] . '.' . self::TARGET_FILE_EXT,
                 true
            );
        }

        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }

    /**
     * @throws Exception
     */
    private function serviceAssist(array $metadata): void
    {
        $serviceYaml = $metadata[self::MD_APP_CONFIG] . self::SERVICES_YAML;
        preg_match("/^(\s*)(?=_defaults:|App\\\\:|App\\\\Controller\\\\:)/m", file_get_contents($serviceYaml), $matches);
        $indent = strlen($matches[1] ?? '');

        if ($indent === 0) {
            throw new Exception('It is not possible to determine the indentation of your ' . $serviceYaml);
        }

        $appConfigServices = Yaml::parseFile($serviceYaml);
        $appParameters =& $appConfigServices['parameters'];
        $appServices   =& $appConfigServices['services'];

        if ($appParameters === null) {
            $appParameters = [];
        }

        $targetNameBundle = implode('|', array_unique(array_column(
            $metadata[self::MD_TARGET_FILE_INFO], self::TARGET_BUNDLE_NAME
        )));

        $isSave = false;
        $configDit = str_replace("/", "\/",self::P_CONFIG);

        $file = (new Finder())->files()
            ->name("/.*\.(ya?ml)$/")
            ->in($metadata[self::MD_TARGET_SRC]);

        foreach ($file as $fileInfo) {
            $path = $fileInfo->getRealPath();
            preg_match("/^.*($targetNameBundle)($configDit).*$/", $path, $matches);
            $nameBundle = $matches[1] ?? null;

            if ($nameBundle) {
                $value = Yaml::parseFile($path);
                $bundleServices = $value['services'] ?? [];
                if (count($bundleServices)) {
                    foreach ($bundleServices as $serviceID => $service) {
                        $class = $service['class'] ?? null;
                        if ($class) {
                            if (array_key_exists($class, $appServices)) {
                                throw new Exception(
                                    "Duplicate found in services. ['$class']: " . print_r($service, true) .
                                    "\nSee $path"
                                );
                            }

                            unset($service['class']);

                            $service[self::MD_SERVICE_ID] = '@' . $serviceID;
                            $appServices[$class] = $service;
                            $isSave = true;
                        }
                    }
                }

                $bundleParameters = $value['parameters'] ?? [];
                if (count($bundleParameters)) {
                    foreach ($bundleParameters as $key => $value) {
                        if (!self::YAML_ALLOW_PREFIX_PARAM && array_key_exists($key, $appParameters)) {
                            throw new Exception(
                                "Duplicate found in parameters. ['$key']: " . print_r($value, true) .
                                "\nSee $path"
                            );
                        }

                        $nameBundle = Sfto5Abstract::to_snake_case(str_replace('Bundle', '', $nameBundle));

                        $paramKey = self::YAML_ALLOW_PREFIX_PARAM
                            ? $nameBundle . self::YAML_PARAM_SEPARATOR_BEFORE . $key
                            : $key;

                        $appParameters[$paramKey] = $value;

                        $isSave = true;
                    }
                }
            }
        }

        if ($isSave) {
            $yaml = Yaml::dump($appConfigServices, 4, $indent);
            file_put_contents($metadata[self::MD_APP_DEV_CACHE] . self::SERVICES_YAML, $yaml);
        }
    }

    private function filesAssist(array $targetRootPaths, array $rootPaths, OutputInterface $output = null): array
    {
        $targetFileInfo = [];
        $trashCollector = [];

        foreach ($targetRootPaths as $group => $targetRootPath) {
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($targetRootPath)
            );

            /**
             * File search
             * @var SplFileInfo $info
             */
            foreach($iterator as $info) {
                if (!$info->isFile()) {
                    continue;
                }

                # ^/Bundle/...
                $withoutRootPath  =
                    str_replace($targetRootPath,DIRECTORY_SEPARATOR,$info->getPath() . DIRECTORY_SEPARATOR);
                $eWithoutRootPath =
                    explode(DIRECTORY_SEPARATOR, trim($withoutRootPath, DIRECTORY_SEPARATOR));

                $fileInfo = [
                    self::GROUP                => $group,
                    self::COPY_FROM            => $info->getRealPath(),
                    self::LEVEL                => count($eWithoutRootPath),
                    self::FILE_EXT             => strtolower($info->getExtension()),
                    self::FILENAME             => $info->getFilename(),
                    self::ABS_ROOT_PATH        => $rootPaths[$group],
                    self::TARGET_ABS_ROOT_PATH => $targetRootPath,
                ];

                if ($group === self::GROUP_TARGET_SRC && self::isBundle($withoutRootPath)) {
                    $fileInfo[self::TARGET_BUNDLE_NAME] = $eWithoutRootPath[0];

                    // Excluded bundle
                    if (!in_array($fileInfo[self::TARGET_BUNDLE_NAME], self::TARGET_BUNDLE, true) &&
                        in_array($fileInfo[self::TARGET_BUNDLE_NAME], self::EXCLUDE_BUNDLE, true)
                    ) {
                        continue;
                    }

                    if (self::ignoreBundleFile($fileInfo)) {
                        $output?->writeln('<info>Ignore file:</info> ' . str_replace(
                                $fileInfo[self::FILENAME],
                                "<error>" . $fileInfo[self::FILENAME] . "</error>",
                                $fileInfo[self::COPY_FROM]
                            )
                        );
                        continue;
                    }

                    foreach (self::EXCLUDE_CONTROLLER_BY_SUFFIX as $suffix) {
                        $pattern = "/^(.*\/Controller\/.*)($suffix)$/";
                        if (preg_match($pattern, $fileInfo[self::COPY_FROM])) {
                            $output?->writeln(
                                '<info>Exclude controller by suffix:</info> ' . preg_replace(
                                    $pattern, "$1<error>$2</error>", $fileInfo[self::COPY_FROM]
                                )
                            );
                            continue 2;
                        }
                    }

                    // Target bundle
                    if (count(self::TARGET_BUNDLE) > 0 &&
                        !in_array($fileInfo[self::TARGET_BUNDLE_NAME], self::TARGET_BUNDLE, true)
                    ) {
                        continue;
                    }

                    // Fix target name bundle
                    if ($newBundleName = self::RENAME_BUNDLE[$fileInfo[self::TARGET_BUNDLE_NAME]] ?? null) {
                        $fileInfo[self::TARGET_BUNDLE_NAME_OLD] = $fileInfo[self::TARGET_BUNDLE_NAME];
                        $fileInfo[self::TARGET_BUNDLE_NAME]     = $newBundleName;
                    }

                    $fileInfo[self::BUNDLE_NAME] = preg_replace(
                        "/^(.*)Bundle$/", '$1', $fileInfo[self::TARGET_BUNDLE_NAME]
                    );

                    // Target bundle directories
                    if ($fileInfo[self::LEVEL] > 1 &&
                        count(self::TARGET_BUNDLE_DIR) > 0 &&
                        !in_array($eWithoutRootPath[1], self::TARGET_BUNDLE_DIR, true)
                    ) {
                        continue;
                    }

                    if ($fileInfo[self::LEVEL] > 1 && $fileInfo[self::FILE_EXT] === 'php') {
                        $firstLevelDirectory = $eWithoutRootPath[1];

                        // Excluded / deprecated directories
                        if (in_array($firstLevelDirectory, self::EXCLUDE_BUNDLE_DIR, true)) {
                            continue;
                        }

                        // Fix name bundle directory
                        $firstLevelDirectory = self::RENAME_BUNDLE_DIR[$firstLevelDirectory] ?? $firstLevelDirectory;

                        $fileInfo[self::TARGET_NAMESPACE] =
                            self::getPathForConstructions(self::AUTOLOAD_PSR_OLD, $withoutRootPath);

                        $inversionWithoutRootPath    = $eWithoutRootPath;
                        $inversionWithoutRootPath[0] = $firstLevelDirectory;
                        $inversionWithoutRootPath[1] = $fileInfo[self::BUNDLE_NAME];

                        $fileInfo[self::TARGET_WITHOUT_ROOT_PATH] = implode(DIRECTORY_SEPARATOR, $eWithoutRootPath);
                        $fileInfo[self::WITHOUT_ROOT_PATH] = implode(DIRECTORY_SEPARATOR, $inversionWithoutRootPath);
                        $fileInfo[self::NAMESPACE] =
                            self::getPathForConstructions(self::AUTOLOAD_PSR_NEW,
                                implode(DIRECTORY_SEPARATOR, $inversionWithoutRootPath)
                            );
                        $fileInfo[self::COPY_TO] = self::getCopyToPath($fileInfo);
                    }

                    if (self::isBundleViews($withoutRootPath)) {
                        $fileInfo[self::GROUP] = self::GROUP_TARGET_BUNDLE_TMP;
                        $fileInfo[self::ABS_ROOT_PATH] = $rootPaths[self::GROUP_TARGET_APP_TMP];

                        $bundleName = $fileInfo[self::TARGET_BUNDLE_NAME_OLD] ?? $fileInfo[self::TARGET_BUNDLE_NAME];
                        $fileInfo[self::TARGET_WITHOUT_ROOT_PATH] = str_replace(
                            DIRECTORY_SEPARATOR . $bundleName . self::P_VIEWS, '/', $withoutRootPath
                        );
                        unset($bundleName);

                        $fileInfo[self::WITHOUT_ROOT_PATH] = self::to_snake_case(
                            $fileInfo[self::BUNDLE_NAME] . $fileInfo[self::TARGET_WITHOUT_ROOT_PATH]
                        );

                        self::fillFilenameSnakeCase($fileInfo);

                        $fileInfo[self::COPY_TO] = self::getCopyToPath($fileInfo);
                    }

                    if (!isset($fileInfo[self::COPY_TO])) {
                        $trashCollector[] = $info->getRealPath();
                        continue;
                    }
                } elseif ($group === self::GROUP_TARGET_APP_TMP && self::isAppViews($targetRootPath)) {
                    $fileInfo[self::TARGET_WITHOUT_ROOT_PATH] = $withoutRootPath;
                    $fileInfo[self::WITHOUT_ROOT_PATH] = self::P_COMMON_TMP_DIR . self::to_snake_case($withoutRootPath);

                    self::fillFilenameSnakeCase($fileInfo);

                    $fileInfo[self::COPY_TO] = self::getCopyToPath($fileInfo);
                } else {
                    $trashCollector[] = $info->getRealPath();
                    continue;
                }

                unset(
                    $fileInfo[self::ABS_ROOT_PATH],
                    $fileInfo[self::TARGET_ABS_ROOT_PATH],
                );

                ksort($fileInfo);
                $targetFileInfo[] = $fileInfo;
            }
        }

        // Sort by depth level. Needed for correct replacement
        usort($targetFileInfo, static function($l, $r) {
            return $r[self::LEVEL] - $l[self::LEVEL];
        });

        $targetFileInfo[self::TRASH_COLLECTOR] = $trashCollector;

        return $targetFileInfo;
    }
}
