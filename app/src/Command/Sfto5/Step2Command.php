<?php

declare(strict_types=1);

namespace App\Command\Sfto5;

use Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class Step2Command extends Sfto5Abstract
{
    private const STEP_NUMBER = 2;

    protected function configure(): void
    {
        $this->setName('sfto5:' . self::STEP_NUMBER);

        $this->setDescription(<<<'EOF'
        - Replace <info>'namespace path'</info> constructions
        - Replace <info>'use path'</info> constructions
          and other mentions
        EOF);
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Step ' . self::STEP_NUMBER . ':');
        $output->writeln($this->getDescription());

        $start = microtime(true);

        $progressBar = new ProgressBar($output);
        $filesystem  = new Filesystem();
        $metadata    = self::getMetadata();
        $targetFileInfo = $metadata[self::MD_TARGET_FILE_INFO];
        $serviceYaml    = $metadata[self::MD_APP_DEV_CACHE] . self::SERVICES_YAML;
        $contentYaml    = file_get_contents($serviceYaml);

        foreach ($progressBar->iterate($targetFileInfo) as $fileInfo) {
            $namespaceTarget = $fileInfo[self::TARGET_NAMESPACE] ?? null;
            $namespace       = $fileInfo[self::NAMESPACE] ?? null;
            if ($namespace) {
                $namespaceBatch   = self::getNamespaceBatch($fileInfo);
                $namespaceDTarget = self::getNamespaceD($namespaceTarget);
                $namespaceD       = self::getNamespaceD($namespace);

                $contentYaml = str_replace(
                    [$namespaceTarget, $namespaceDTarget], [$namespace, $namespaceD], $contentYaml
                );

                foreach ($targetFileInfo as &$refFileInfo1) {
                    $filename = self::getPathToTargetFile($refFileInfo1);
                    $content = $refFileInfo1[self::CONTENT] ?? file_get_contents($filename);

                    $content = str_replace(
                        [$namespaceTarget, $namespaceDTarget], [$namespace, $namespaceD], $content
                    );

                    foreach ($namespaceBatch as $lNs => $rNs) {
                        $content = str_replace($lNs, $rNs, $content);
                    }

                    $refFileInfo1[self::CONTENT] = $content;
                }
            }
        }

        foreach ($targetFileInfo as $fileInfo) {
            if (isset($fileInfo[self::CONTENT])) {
                $filesystem->dumpFile(self::getPathToTargetFile($fileInfo), $fileInfo[self::CONTENT]);
            }
        }

        file_put_contents($serviceYaml, $contentYaml);

        $output->writeln(' - ' . round(microtime(true) - $start, 2) . ' s.');
        $output->writeln(self::passedSteps(self::STEP_NUMBER));

        return 0;
    }
}
