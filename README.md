# Sfto5 - Auto migration of bundles Symfony 3.4 to 5.2

***- ⚠️ You better come here [rectorphp/rector (Instant Upgrades and Automated Refactoring)](https://github.com/rectorphp/rector)***

---

***- In my case, it was a transition from 3.4 to 5.2 and PHP 8.***

---

This is a humble tool for transferring `Sf3.4` bundles to the new `Sf5.2` catalog structure.
These tools are specially designed as commands for the console so you can tailor them to
your project when needed.

---

***- This will not give you 100% transition, but it will take away some routine
and will make small useful edits.***

---

### Description of tools
[Command/Sfto5/:][Sfto5]
- [Config][Config] - settings for projects, defining targets and exclusions. Important ⚠️
- [StartCommand][Start] - preparation of directories, cache, temporary changes gitignore
- [StepConveyorCommand][Conveyor] - performs all steps automatically
- [Step1Command][Step1]
- [Step2Command][Step2]
- [Step3Command][Step3]
- [Step4Command][Step4]
- [Step5Command][Step5]
- [Step6Command][Step6]
- [Step7Command][Step7]
- [FinalCommand][Final]
- [CleanCommand][Clean]

---

### Preparing old project

- If PhpStorm. Right click on directory `/src`, then click on menu `optimize imports`.
  This will remove unused imports. Later it will be required.
- If possible. Get rid of groupings that start at the root of the bundle. 
  Bad example: 
  ```php
  use App\TestFeatureBundle\{
      Entity\Class, 
      Repository\Class, 
      Service\Class
  };
  ```
  Need to bring
  ```php
  use App\TestFeatureBundle\Entity\Class;
  use App\TestFeatureBundle\Repository\Class;
  use App\TestFeatureBundle\Service\Class;
  ```

### Preparing new project

###### Step 1: 

- Create a new project. Here is all the information you need 
  [Installing & Setting up the Symfony Framework][Sf:setup]
- Run *(optional)*
  ```shell 
  $ symfony composer require symfony/filesystem;
  $ symfony composer require symfony/finder;
  $ symfony composer require symfony/yaml;
  ```

###### Step 2:

- Move `app/src/Command/Sfto5` to your new project
- Run
  ```shell
  $ bin/console sfto5:start;
  ```
  You should receive a similar report:
  
  ![i-1]

###### Step 3:

***- You should now have a directory `target/src`***
- For PhpStorm: *(optional / recommend)*
  - Mark directory `target/src` excluded. What would not prevent the indexing
  - Open `Setting|Editor|FileTypes` and add `*.php.target` associations pattern for PHP.
    Later this will give you early access for code analysis

###### Step 4:

- In `target/src` you need to copy your bundles from the old project
    
###### Step 5: ⚠️

- Be sure to check the settings [Sfto5/ConfigInterface.php]([Config]) before continuing

###### Step 6:

- Run `bin/console sfto5:1;` or `bin/console sfto5:step-conveyor;`
- For those who are in a hurry :) `sfto5:step-conveyor` 
  will perform all steps in automatic mode

###### Step 7.1: *(optional)*

- ***- Until the finale, you can run the cleaning.*** Run
  ```shell
  $ bin/console sfto5:clean --collapse=t;
  ```
  will remove `*.target` files, cache, tags, etc.
  and you have to start again with `sfto5:start`

###### Step 7.2:

- ***- If everything went well for you.*** Run
  ```shell
  $ bin/console sfto5:final;
  ```
  will remove the `*.target` extension from files, if you need to return
  - Run `bin/console sfto5:final --rollback=t;`

- ***- in the process such errors may appear***
  
  ![i-2]
  
  The files will have to be opened and corrected manually, 
  but if you are still satisfied with this, run
  ```shell
  $ bin/console sfto5:final --cns=f;
  ```
  `--cns=f` this will disable checking and the `*.target` extension will be removed

###### Step 8:

- ***- If you are done.*** Run
  ```shell
  $ bin/console sfto5:clean;
  ```
  This will remove temporary tags `# SFTO-`, `@ SFTO-`, metadata, cache, etc.

---

[Sf:setup]: https://symfony.com/doc/current/setup.html

[Config]: https://gitlab.com/barbozze/sfto5/-/blob/master/app/src/Command/Sfto5/ConfigInterface.php
[Sfto5]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5
[Start]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/StartCommand.php
[Conveyor]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/StepConveyorCommand.php
[Step1]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step1Command.php
[Step2]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step2Command.php
[Step3]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step3Command.php
[Step4]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step4Command.php
[Step5]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step5Command.php
[Step6]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step6Command.php
[Step7]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/Step7Command.php
[Final]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/FinalCommand.php
[Clean]: https://gitlab.com/barbozze/sfto5/-/tree/master/app/src/Command/Sfto5/CleanCommand.php

[i-1]: ./md-illustration/img_0.png
[i-2]: ./md-illustration/img_1.png